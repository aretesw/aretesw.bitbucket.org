# arete.ws #

Estos son los archivos del sitio estático de Areté. Puedes visitar el sitio en http://arete.ws

Puedes hacer pull requests a este sitio y pedirle a @aesc_arete que los integre.

El sitio es de todos. Colabora para hacerlo mejor.